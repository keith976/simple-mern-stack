import express from 'express';
import mongoose from 'mongoose';
import bodyparser from 'body-parser';
import routes from './routes/routes';
import cors from 'cors';

const app = express();
const PORT = 3000;

// Mongo Connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/mernDB',{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// BodyParser Setup
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

// CORS setup
app.use(cors());

routes(app);

app.get('/', (req, res) => {
  res.send(`Our application is running on port ${PORT}`);
})

app.listen(PORT, () => {
  console.log(`Your server is running on port ${PORT}`);
})