import { 
  addNewPlayer, 
  getPlayers,
  getPlayerByID,
  updatePlayer,
  deletePlayer
} from '../controllers/playerController';

const routes = (app) => {
  app.route('/players')

      //GET endpoints
      .get(getPlayers)

      //POST endpoints
      .post(addNewPlayer);

  app.route('/player/:PlayerID')

      //GET endpoints
      .get(getPlayerByID)

      //DELETE endpoints
      .delete(deletePlayer)
      
      //PUT endpoints
      .put(updatePlayer);

}

export default routes;